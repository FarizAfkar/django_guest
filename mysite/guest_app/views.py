from django.shortcuts import render, redirect
from .form import GuestForm


def guest(request):
    if request.method == 'POST':
        form = GuestForm(request.POST or None)
        if form.is_valid():
            form.save()
        return redirect(guest)
    else:
        return render(request, 'index.html')
