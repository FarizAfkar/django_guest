from django import forms
from .models import Guestbook


class GuestForm(forms.ModelForm):
    class Meta:
        model = Guestbook
        fields = ['nama', 'alamat', 'email']
