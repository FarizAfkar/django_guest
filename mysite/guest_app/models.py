from django.db import models


class Guestbook(models.Model):
    nama = models.CharField(max_length=40)
    alamat = models.CharField(max_length=40)
    email = models.EmailField(max_length=50)
